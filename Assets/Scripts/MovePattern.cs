﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MovePattern : MonoBehaviour {
	public float speed;
	public List<Transform> movingObjects;

	private List<Vector3> initialPositions = new List<Vector3>();
	private List<int> movingToIndex = new List<int>();

	void Start () {
		for (int i=0;i<movingObjects.Count;i++){
			initialPositions.Add (movingObjects[i].position);
			if ( i+1 == movingObjects.Count){
				movingToIndex.Add (0);
			}else{
				movingToIndex.Add (i+1);
			}
		}
	}

	void Update () {
		for (int i=0;i<movingObjects.Count;i++){
			Vector3 movingTowards = initialPositions[movingToIndex[i]];
			float distance = Vector3.Distance(movingObjects[i].transform.position,movingTowards);
			movingObjects[i].transform.position = Vector3.MoveTowards(movingObjects[i].transform.position,movingTowards,speed*Time.deltaTime);
			if (movingObjects[i].transform.position == movingTowards){
				movingToIndex[i]++;
				if (movingToIndex[i] == movingObjects.Count){
					movingToIndex[i] = 0;
				}
				if (distance<speed*Time.deltaTime){
					movingObjects[i].transform.position = Vector3.MoveTowards(movingObjects[i].transform.position,movingTowards,speed*Time.deltaTime - distance);
				}
			}
		}
	}
}
