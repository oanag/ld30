﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
	private const float INITIAL_VELOCITY = 2;

	void Start () {
		rigidbody2D.velocity = transform.up*INITIAL_VELOCITY;
	}

	void OnCollisionEnter2D(Collision2D coll) {
		Destroy (this.gameObject);
	}
	
	void OnTriggerEnter2D(Collider2D other) {
		Destroy (this.gameObject);
	}

}
