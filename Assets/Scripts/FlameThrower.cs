﻿using UnityEngine;
using System.Collections;

public class FlameThrower : MonoBehaviour {
	private const float FIRE_INTERVAL = 5;

	public ParticleSystem particles;
	public Collider2D particleCollider;
	public AudioSource audio;

	private float lastFired;
	Vector3 colliderPos;
	Vector3 colliderScale;
	float lifetime;

	void Start () {
		particleCollider.enabled = false;
		colliderPos = particleCollider.transform.position;
		colliderScale = particleCollider.transform.localScale;
		lifetime = particles.startLifetime;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.timeSinceLevelLoad - lastFired>FIRE_INTERVAL){
			lastFired = Time.timeSinceLevelLoad;
			audio.Play();
			particles.Play();
			StartCoroutine(FireCr ());
		}
	}

	IEnumerator FireCr(){
		float animDuration = 1f;
		float time=0;
		particleCollider.enabled = true;
		while (time<animDuration){
			time+= Time.deltaTime;
			float progress = time/animDuration;
			particleCollider.transform.position = Vector3.Lerp (transform.position,colliderPos,progress);
			Vector3 scale = particleCollider.transform.localScale;
			scale.x = Mathf.Lerp (0.1f,colliderScale.x,progress);
			particleCollider.transform.localScale = scale;
			particles.startLifetime = Mathf.Lerp (0.4f,lifetime,progress);
			yield return null;
		}
		yield return new WaitForSeconds(0.3f);
		time = 0;
		while (time<animDuration){
			time+= Time.deltaTime;
			float progress = time/animDuration;
			particleCollider.transform.position = Vector3.Lerp (colliderPos,transform.position,progress);
			Vector3 scale = particleCollider.transform.localScale;
			scale.x = Mathf.Lerp (colliderScale.x,0.1f,progress);
			particleCollider.transform.localScale = scale;
			particles.startLifetime = Mathf.Lerp (lifetime,0.4f,progress);
			yield return null;
		}
		particleCollider.enabled = false;
	}
}
