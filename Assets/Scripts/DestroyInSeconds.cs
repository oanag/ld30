﻿using UnityEngine;
using System.Collections;

public class DestroyInSeconds : MonoBehaviour {
	public float seconds;

	void Start () {
		StartCoroutine (DestroyInSecondsCr());
	}

	IEnumerator DestroyInSecondsCr(){
		yield return new WaitForSeconds(seconds);
		Destroy (gameObject);
	}


}
