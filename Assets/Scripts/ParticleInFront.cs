﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ParticleInFront : MonoBehaviour {

	void Start () {
		particleSystem.renderer.sortingLayerName = "Particles";
	}
}
