﻿using UnityEngine;
using System.Collections;

public class Wormhole : MonoBehaviour {
	public Wormhole linkedWormhole;
	public ParticleSystem linkParticles;

	void Start () {
		if (linkParticles!=null && linkedWormhole != null){
			Vector3 moveDirection = linkedWormhole.transform.position - transform.position; 
			float angle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
			transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
			float distance = (linkParticles.transform.position-linkedWormhole.transform.position).magnitude;
			linkParticles.startLifetime = distance/linkParticles.startSpeed;
		}else{
			Debug.LogWarning ("Unlinked wormhole", this.gameObject);
		}
	}
}
