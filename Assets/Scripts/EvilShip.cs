﻿using UnityEngine;
using System.Collections;

public class EvilShip : MonoBehaviour {
	private const float BULLET_INTERVAL = 2; //in seconds

	public GameObject bulletPrefab;
	public AudioSource shotSound;

	private Player playerShip;
	private float timeLastBullet;

	void Start () {
		playerShip = Player.Instance;
		timeLastBullet = 0;
	}

	void Update () {
		if (playerShip!=null){
			Vector3 dir = playerShip.transform.position - transform.position;
			float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg -90;
			transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
			if (Time.timeSinceLevelLoad - timeLastBullet>BULLET_INTERVAL){
				timeLastBullet = Time.timeSinceLevelLoad;
				shotSound.Play();
				GameObject bullet = (GameObject) Instantiate(bulletPrefab, transform.position + transform.up*0.8f, transform.rotation);
			}
		}
	}
}
