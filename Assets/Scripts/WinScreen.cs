﻿using UnityEngine;
using System.Collections;
using System;

public class WinScreen : MonoBehaviour {
	public dfTextbox prevRecord;
	public dfTextbox currentScore;
	public dfTextbox newRecord;

	void Start () {
		Time.timeScale = 0;
		float record = PlayerPrefs.GetFloat("PrevRecord",0);
		Debug.Log ("RECORD "+record);
		if (record>0){
			TimeSpan ts = TimeSpan.FromSeconds(record);
			prevRecord.Text = "Previous record: "+Math.Floor (ts.TotalMinutes).ToString("00")+":"+ts.Seconds.ToString("00");
		}else{
			prevRecord.gameObject.SetActive(false);
		}
		TimeSpan ts2 = TimeSpan.FromSeconds(Time.timeSinceLevelLoad);
		currentScore.Text = "Time: "+Math.Floor (ts2.TotalMinutes).ToString("00")+":"+ts2.Seconds.ToString("00");
		if (record>Time.timeSinceLevelLoad || record == 0){
			PlayerPrefs.SetFloat("PrevRecord",Time.timeSinceLevelLoad);
			PlayerPrefs.Save();
		}
		if (record>Time.timeSinceLevelLoad){
			newRecord.gameObject.SetActive(true);
		}else{
			newRecord.gameObject.SetActive(false);
		}
		GA.API.Design.NewEvent("GameEnded",Time.timeSinceLevelLoad); 
	}
	
	public void RestartButton(){
		GA.API.Design.NewEvent("Win:Restart",Time.timeSinceLevelLoad); 
		Application.LoadLevel("Game");
		Time.timeScale = 1;
	}
	
	public void MainMenuButton(){
		GA.API.Design.NewEvent("Win:MainMenu",Time.timeSinceLevelLoad); 
		Application.LoadLevel("MainMenu");
		Time.timeScale = 1;
	}
}
