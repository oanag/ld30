﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	private const float SPEED = 5;
	private const float MAX_SPEED = 5;
	private const float ROTATION_SPEED = 200;

	public AudioSource engineSound;
	public ParticleSystem engineParticles;
	public GameObject deathPrefab;
	public Camera followingCamera;
	public AudioSource wormholeJumpAudio;
	public GameObject collectStarPrefab;

	private bool isRunning = false;
	public bool IsDead {
		get; private set;
	}

	private GameObject recentlyTeleportedWormhole = null;

	public static Player Instance{
		get; private set;
	}

	void Awake(){
		Instance = this;
	}

	void Start(){
		IsDead = false;
	}

	void FixedUpdate () {
		if (IsDead){ 
			return;
		}
		rigidbody2D.AddForce(transform.up*SPEED*Input.GetAxis("Vertical"));
		if (rigidbody2D.velocity.magnitude>MAX_SPEED){
			float crtMagnitude = rigidbody2D.velocity.magnitude;
			Vector2 velocity = rigidbody2D.velocity;
			velocity.x *= MAX_SPEED/crtMagnitude;
			velocity.y *= MAX_SPEED/crtMagnitude;
			rigidbody2D.velocity = velocity;
		}

		if (Input.GetAxis("Vertical")!=0 && !isRunning){
			engineSound.Play ();
			engineParticles.Play();
			isRunning = true;
		}
		if (Input.GetAxis("Vertical")==0 && isRunning){
			engineSound.Stop();
			engineParticles.Stop();
			isRunning = false;
		}

		rigidbody2D.angularVelocity = -ROTATION_SPEED*Input.GetAxis("Horizontal");

		Vector3 cameraPos = followingCamera.transform.position;
		cameraPos.x = transform.position.x;
		cameraPos.y = transform.position.y;
		followingCamera.transform.position = cameraPos;
	}

	void OnTriggerEnter2D(Collider2D other){
		GA.API.Design.NewEvent("TriggerEnter:"+other.gameObject.name, Time.timeSinceLevelLoad, transform.position); 
		if (other.CompareTag("Instakill")){
			IsDead = true;
			engineSound.Stop();
			engineParticles.Stop();
			rigidbody2D.velocity = Vector2.zero;
			rigidbody2D.angularVelocity = 0;
			Instantiate (deathPrefab,transform.position,transform.rotation);
			Destroy (this.gameObject);
			Level.ShowLoseScreen();
		}else if (other.CompareTag("Wormhole")){
			if (recentlyTeleportedWormhole != null && recentlyTeleportedWormhole == other.gameObject){
				return;
			}
			Wormhole wormhole = other.GetComponent<Wormhole>();
			transform.position = wormhole.linkedWormhole.transform.position;
			recentlyTeleportedWormhole = wormhole.linkedWormhole.gameObject;
			wormholeJumpAudio.Play();
		}else if (other.CompareTag("Star")){
			Level.CollectStar();
			Instantiate (collectStarPrefab,other.transform.position,other.transform.rotation);
			Destroy(other.gameObject);
		}
	}

	void OnTriggerExit2D(Collider2D other){
		if (recentlyTeleportedWormhole != null && recentlyTeleportedWormhole == other.gameObject){
			recentlyTeleportedWormhole = null;
		}
	}

}
