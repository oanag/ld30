﻿using UnityEngine;
using System.Collections;

public class DestroyOnCollision : MonoBehaviour {
	
	void OnCollisionEnter2D(Collision2D coll) {
		Destroy (this.gameObject);
	}
	
	void OnTriggerEnter2D(Collider2D other) {
		Destroy (this.gameObject);
	}
}
