﻿using UnityEngine;
using System.Collections;

public class LoseScreen : MonoBehaviour {
	private const float SECONDS_TO_TRANSITION=5;
	private float time = 0;
	private float prevTime = 0;
	private bool transitioned = false;

	public dfTextbox restartingInLabel;

	void OnEnable () {
		Time.timeScale = 0;
		time = 0;
		prevTime = Time.unscaledTime;
	}
	void OnDisable () {
		Time.timeScale = 1;
	}

	void Update(){
		float deltaTime = Time.unscaledTime - prevTime;
		prevTime = Time.unscaledTime;
		if (time + deltaTime<SECONDS_TO_TRANSITION){
			time+= deltaTime;
			restartingInLabel.Text = "Restarting in "+Mathf.Floor(SECONDS_TO_TRANSITION - time)+"...";
		}else if (!transitioned){
			RestartButton();
		}
	}

	public void RestartButton(){
		GA.API.Design.NewEvent("Lose:Restart",Time.timeSinceLevelLoad); 
		Application.LoadLevel("Game");
		Time.timeScale = 1;
	}
	
	public void MainMenuButton(){
		GA.API.Design.NewEvent("Lose:MainMenu",Time.timeSinceLevelLoad); 
		Application.LoadLevel("MainMenu");
		Time.timeScale = 1;
	}
}
