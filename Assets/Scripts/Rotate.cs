﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {
	public float rotateSpeed;

	void Update () {
		rigidbody2D.MoveRotation(rigidbody2D.rotation + rotateSpeed * Time.deltaTime);
	}
}
