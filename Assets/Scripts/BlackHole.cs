﻿using UnityEngine;
using System.Collections;

public class BlackHole : MonoBehaviour {
	private const float GRAVITY = 2f;
	private const float MAX_DISTANCE = 5f;

	private Player playerShip;
	void Start () {
		playerShip = Player.Instance;
	}

	void FixedUpdate () {
		if (playerShip == null) return;
		Vector3 dir = transform.position - playerShip.transform.position;
		float distance = dir.magnitude;
		if (distance>MAX_DISTANCE) return;
		dir.Normalize();
		playerShip.rigidbody2D.AddForce (new Vector2(dir.x,dir.y)*GRAVITY*distance/MAX_DISTANCE);
	}
}
