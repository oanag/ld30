﻿using UnityEngine;
using System.Collections;
using System;

public class Level : MonoBehaviour {
	
	public GameObject winScreen;
	public GameObject loseScreen;
	public Player player;

	public static int MaxStarCount{
		get; private set;
	}

	public static int CrtStarCount{
		get; private set;
	}

	public static Level Instance{
		get; private set;
	}

	public static float SecondsSinceStart{
		get; private set;
	}

	void Start () {
		CrtStarCount = 0;
		GameObject [] stars = GameObject.FindGameObjectsWithTag("Star");
		MaxStarCount = stars.Length;
		SecondsSinceStart = 0;
		Instance = this;
	}


	bool isPaused = false;

	void Update(){
		if (Input.GetKeyDown (KeyCode.P)){
			if (isPaused){
				Time.timeScale = 1;
				isPaused = false;
			}else{
				Time.timeScale = 0;
				isPaused = true;
			}
		}
	}

	void FixedUpdate(){
		if (!player.IsDead){
			SecondsSinceStart += Time.deltaTime;
		}
	}

	public static void CollectStar(){
		CrtStarCount++;
		if (CrtStarCount == MaxStarCount){
			Instance.winScreen.SetActive(true);
		}
	}

	public static void ShowLoseScreen(){
		Instance.StartCoroutine(ShowLoseScreenCr());
	}

	static IEnumerator ShowLoseScreenCr(){
		yield return new WaitForSeconds(2f);
		Instance.loseScreen.SetActive(true);
	}
}
