﻿using UnityEngine;
using System.Collections;

public class ObjectSpawner : MonoBehaviour {
	
	public float SpawnInterval = 1; //in seconds
	
	public GameObject spawnPrefab;
	private float timeLastSpawn;


	void Update () {
		if (Time.timeSinceLevelLoad - timeLastSpawn>SpawnInterval){
			timeLastSpawn = Time.timeSinceLevelLoad;
			GameObject bullet = (GameObject) Instantiate(spawnPrefab, transform.position + transform.up*0.8f, transform.rotation);
		}
	}
}
