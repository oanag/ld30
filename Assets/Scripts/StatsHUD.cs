﻿using UnityEngine;
using System.Collections;
using System;

public class StatsHUD : MonoBehaviour {
	public dfTextbox starsCollectedLabel; 
	public dfTextbox timeSpentLabel;

	void Update () {
		starsCollectedLabel.Text = Level.CrtStarCount+"/"+Level.MaxStarCount;
		TimeSpan span = TimeSpan.FromSeconds(Level.SecondsSinceStart); 
		timeSpentLabel.Text = Math.Floor(span.TotalMinutes).ToString("00")+":"+span.Seconds.ToString("00");
	}
}
