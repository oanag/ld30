Note: to get this working, a copy of Daikon Forge GUI needs to be added. Not included in the repository due to the closed source network of the Unity asset.

For an older version with the UI running in Unity 4.6 with uGUI, you can roll back to before the change.